.. _dao:

DAO
===

The DAO provides de-centralised governance by:
-  Allowing new digital autonomous organisations to be created using existing legal entities. For example, digital mutual societies or digital joint stock companies.
-  Provides de-centralised consensus in order to remove the need for a central operator, owner or authority. This allows cordite to be more resilient, cheaper, agile and private than the incumbent financial infrastructure.
-  Facilitates the funding, building and organising of decentralised applications.
-  Encapsulates voting, raising issues, marshaling changes to the system and deciphering economic models.

Why are we interested in DAOs?
------------------------------

It is really hard to bring together a group of organisations to fund and
build a decentralised application that they would all like to use.

We have repeatedly seen that if you try and build a “blockchain app” and
sell it, no one is interested - and you can't have a blockchain app on
your own! However if you talk to other institutions to investigate
co-creation, then they are often really interested. However actually
getting a disparate group of organisations together to fund the building
of a decentralised app is really hard.

-  getting budgeting cycles aligned is hard
-  getting experts together is hard
-  who would actually build the app?
-  who will make changes?
-  who chooses which changes to build next?
-  who will get those changes deployed safely to many institutions'
   nodes?

However imagine if we spin up a decentralised autonomous organisation -
a DAO. We could create governance, economic and membership models, in
code, that make it easier to bring organisations together to:

-  fund the building of a decentralised app
-  propose, and vote for, changes to the app
-  propose, and vote for, changes to the dao itself - e.g. changes to
   voting or membership rules

Some DAOs may:

-  offer incentives for joining the dao early
-  have rules for distributing proceeds for deploying the system to
   another geography

A **Decentralised Autonomous Orgnisation (DAO)** is an organisation that
is governed by rules based on computer code or network protocols and its
decisions are made through the voting of its members.

We think that DAOs represent a really elegant way to bring organisations
together to fund the building, and running, of decentralised
applications between institutions much easier, without having to resort
to a centralised company sitting in the middle monetising this.

Open source foundations represent existing example that is not that far
away from what we are talking about here.

For an example of DAO, see
`Dash <https://www.dash.org/forum/threads/how-does-the-dash-dao-work.9560/>`__

DAO : Membership
~~~~~~~~~~~~~~~~

**Julian** starts a gang with his brother **Dick** called the **Famous
Five**. **Dick** proposes their sister **Anne** joins and **Julian**
accepts. **Anne** proposes their friend **Georgina**. Both **Dick** and
**Julian** accept. **Georgina** proposes her dog **Timmy** joins but
none of the others accept her proposal. A simple test of the Cordite DAO
functionality. Each actor can run a separate Cordite node where
**Cordite** is the collection of nodes.

.. mermaid::

    sequenceDiagram 
        Julian->>FamousFive DAO: createDaoWithName(FamousFive) 
        Julian->>FamousFive DAO: createNewMemberProposal(FamousFive, Dick) 
        Julian->>FamousFive DAO: acceptNewMemberProposal(FamousFive, Dick) 
        Dick->>FamousFive DAO: createNewMemberProposal(FamousFive, Anne) 
        Julian->>FamousFive DAO: voteForNewMemberProposal(FamousFive, Anne) 
        Dick->>FamousFive DAO: acceptForNewMemberProposal(FamousFive, Anne) 
        Anne->>FamousFive DAO: createNewMemberProposal(FamousFive, Georgina) 
        Julian->>FamousFive DAO: voteForNewMemberProposal(FamousFive, Georgina) 
        Dick->>FamousFive DAO: voteForNewMemberProposal(FamousFive, Georgina) 
        Anne->>FamousFive DAO: acceptForNewMemberProposal(FamousFive, Georgina) 
        Georgina->>FamousFive DAO: createNewMemberProposal(FamousFive, Timmy)

|

DAO: Proposals
~~~~~~~~~~~~~~~

**Julian** proposes to his gang **FamousFive** that they head to
**Treasure Island**. **Anne** proposes an alternative to **Go Off in a
Caravan**. **Dick** and **Georgina** vote for the **Treasure Island**
proposal. **Anne** finds her proposal has been rejected, while
**Julian**\ 's has been accepted. The **FamousFive** head to **Treasure
Island**. A simple test of the Cordite DAO functionality. Each actor can
run a separate Cordite node where **FamousFive** is a distributed
autonomous organisation (DAO) running on Cordite.


.. mermaid::

    sequenceDiagram 
        Julian->>FamousFive DAO: createProposal(Five on a Treasure Island) 
        Anne->>FamousFive DAO: createProposal(Five Go Off in a Caravan) 
        Dick->>FamousFive DAO: normalProposalsFor(FamousFive)
        Dick->>FamousFive DAO: voteForProposal(Five on a Treasure Island)
        Georgina->>FamousFive DAO: normalProposalsFor(FamousFive)
        Georgina->>FamousFive DAO: voteForProposal(Five on a Treasure Island)
        Anne->>FamousFive DAO: acceptProposal(Five Go Off in a Caravan) = REJECTED 
        Julian->>FamousFive DAO: acceptProposal(Five on a Treasure Island) = ACCEPTED 

|

Apologies to `Enid
Blyton <https://en.wikipedia.org/wiki/Enid_Blyton>`__, the author of
`The Famous Five
Series <https://en.wikipedia.org/wiki/The_Famous_Five_(novel_series)>`__.
The characters and stories are based on this series.


CLI Demonstration
-----------------

:ref:`Click here for the CLI Demonstration <daocli>`