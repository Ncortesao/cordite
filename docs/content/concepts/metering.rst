.. _metering:

########
Metering
########

Metering incentivises people to fairly participate in de-centralised organisations by:
• Incentivising parties to run metering notaries that can receive payments for notarisation.
• Accommodating a variety of economic models, such as PayGo and other customised structures.
• Handling invoice and dispute resolution.

Incentives — We all need them and we all use them. From how you are remunerated for your time to the ice cream you give your kids to reward them for good behaviour; distributed ledgers are no exception. Since there is no central party governing the network of a distributed ledger, we rely upon the participants to provide services to keep the show on the road, and no one does this for free.

Different distributed ledgers will need different incentives to reward participants providing services to the network. More than one economic model is expected to emerge in order to server the different applications of a distributed ledger technology.

Lovely Rita, meter maid
-----------------------

Corda introduced us to notaries to verify the transactions. Building on this, Cordite introduces the concept of Metering Notaries. Metering notaries verify the transaction and sends an invoice to the party originating the transaction including the notary’s fees, much like a gas or electric utility company does. The transacting party can accept or dispute these invoices. They can pay the invoice using :ref:`Cordite DGL tokens <dgl>`. Metering notaries should be able to refuse to verify transactions for parties that do not pay their invoices.

Metering fees are collected in a :ref:`Cordite DAO <dao>` (aka `Digital Mutual <https://medium.com/corda/the-return-of-the-dao-88a93b64ec9f>`__) and distributed back to the metering notaries. This provides the ability for pools of metering notaries to operate where they share the fees and the DAO provides governance over the metering notary pool in order to allow the metering pool to evolve over time. The DAO itself may also take a share of the fees in order to provide services

Metering Notaries have their own transactions verified by a guardian notary. The guardian notaries can also be run as Metering Notaries to create two inter-locked sets of metering notaries checking each other’s transactions in a de-centralised model. Depending on preference or application, you can adopt a variety of pricing models for your Dapp. It allows organisations with differing economic needs and applications to adapt respectably.

The key point here, is that Metering Notaries represent an elegant and versatile method to provide the necessary incentives while maintaining the `benefits of de-centralisation <https://medium.com/@rickcrook/the-case-for-de-centralisation-1ac14935a3fc>`__.

Below is a story showcasing a simple token transaction and how metering
works.

Metering: Token Transaction
---------------------------

**Georgina** sends a token to **Dick**, and the token transaction is
notarised by a **Metering Notary** who charges a fee and issues a
Metering Invoice to **Georgina**. The Metering Invoice transaction is
then notarised by a **Guardian Notary** before the Metering Invoice is
paid by **Georgina** with tokens to the **DAO**. The **DAO** then
distributes the funds according to the **DAO** rules.

.. mermaid::

    sequenceDiagram 
        Georgina ->> Dick: Sends a Token 
        Georgina ->> MeteringNotary : Requests Notarisation for Token Transaction (Tx)
        MeteringNotary ->> MeteringNotary : Notarises Token Tx 
        MeteringNotary ->> Georgina: Sends Metering Invoice for Token Tx 
        MeteringNotary ->> GuardianNotary : Requests Notarisation for Metering Invoice Tx
        GuardianNotary ->> GuardianNotary : Notarises Metering Invoice Tx
        Georgina ->> Dao : Pays the Metering Invoice with Tokens 
        Dao ->> Dao : Looks up the Dao rules on distributing funds 
        Dao ->> MeteringNotary : Sends % of invoice fees 
        Dao ->> Dao : Puts % of fees in Dao Foundation Fund 
        Dao ->> GuardianNotary : Sends % of invoice fees 
    
|

Apologies to `Enid
Blyton <https://en.wikipedia.org/wiki/Enid_Blyton>`__, the author of
`The Famous Five
Series <https://en.wikipedia.org/wiki/The_Famous_Five_(novel_series)>`__.
The characters and stories are based on this series.

CLI Demonstration
-----------------

:ref:`Click here for the CLI Demonstration <meteringcli>`