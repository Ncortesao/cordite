Concepts
========

.. toctree::
   :hidden:

   concepts/dgl
   concepts/dao
   concepts/metering
   concepts/foundation


:ref:`dgl`

ledger with accounts

.. image:: /images/dgl.png
   :target: ../content/concepts/dgl.html
   :height: 150px
   :width: 150 px

:ref:`dao`
 
governance

.. image:: /images/DAO3.png
   :target: ../content/concepts/dao.html
   :height: 150px
   :width: 150 px

:ref:`metering`

economics

.. image:: /images/Meter_icon.png
   :target: ../content/concepts/metering.html
   :height: 150px
   :width: 150 px
