Cordite
======================


.. toctree::
   :hidden:
   :glob:

   Overview <content/overview>
   content/concepts
   Cordite Test <content/cordite_test>
   content/contributing
   FAQ <content/faq>


Cordite provides de-centralised governance and economic services to mutually distrusting corporations wishing to solve distributed challenges in their industries.

Cordite is different things to different people. You have to start with the aims of Cordite to understand why:

*  Wide spread adoption of https://corda.net

*  Build awareness and understanding of the benefits of Corda

*  Encourage use of Corda by demonstrating how it will help improve your business

*  Support thought leaders in using Corda

Firstly it is a community of contributors who believe in these aims and hang out on the #cordite channel on Corda slack - https://cordaledger.slack.com/messages/cordite/

Secondly it is an established public Corda network which you can join - https://network-map-test.cordite.foundation/

Thirdly it is packaged into a simple Docker image so you can run a Corda node easily with the Cordite CordApps pre-packaged - https://hub.docker.com/r/cordite/cordite/

Fourth it is a set of CordApps which you can use with other CordApps to provide tokens, digital mutuals and metering in your own eco-systems - https://search.maven.org/search?q=g:io.cordite

We. Are. Cordite.  https://cordite.foundation/ @we_are_cordite
Built on Corda, Cordite is open source, regulatory friendly, enterprise ready and finance grade.

You are warmly welcome to contribute by improving these docs or anyway you can - <content/contributing>

