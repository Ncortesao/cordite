/**
 *   Copyright 2018, Cordite Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package io.cordite.metering.service

import com.fasterxml.jackson.annotation.JsonIgnore
import io.cordite.commons.utils.Resources
import io.vertx.core.json.Json
import io.vertx.core.json.JsonObject
import net.corda.core.utilities.loggerFor


const val METERING_SERVICE_CONFIG_FILENAME = "metering-service-config.json"
const val DEFAULT_METERING_REFRESH_INTERVAL = 2000L

data class MeteringServiceConfig(val meteringRefreshInterval: Long = DEFAULT_METERING_REFRESH_INTERVAL,
                                 val meteringServiceType: MeteringServiceType = MeteringServiceType.SingleNodeMeterer,
                                 val daoPartyName: String = "",
                                 val meteringNotaryAccountId: String = "",
                                 val meteringPartyName: String = "",
                                 val daoName: String ="") {
  companion object {
    private fun getConfigFromFile(meteringServiceConfigFileName: String): JsonObject {
      try {
        JsonObject(Resources.loadResourceAsString(meteringServiceConfigFileName)).let {
          log.info("found metering config file $meteringServiceConfigFileName")
          return it
        }
      } catch (_: Throwable) {
        log.warn("could not find the metering config file $METERING_SERVICE_CONFIG_FILENAME")
        return JsonObject()
      }
    }

    @JsonIgnore
    private val log = loggerFor<MeteringServiceConfig>()

    fun loadConfig(meteringServiceConfigFileName: String = METERING_SERVICE_CONFIG_FILENAME): MeteringServiceConfig {

      val configFromFile = getConfigFromFile(meteringServiceConfigFileName)
      return Json.decodeValue<MeteringServiceConfig>(configFromFile.encode(), MeteringServiceConfig::class.java)
    }
  }
}

enum class MeteringServiceType {
  SingleNodeMeterer,
  BftSmartMeterer,
  RaftMeterer
}

