/**
 *   Copyright 2018, Cordite Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package io.cordite.metering.service

import net.corda.core.node.AppServiceHub
import net.corda.core.node.services.CordaService
import net.corda.core.serialization.CordaSerializable
import net.corda.core.serialization.SingletonSerializeAsToken
import net.corda.core.utilities.loggerFor
import net.corda.nodeapi.internal.addShutdownHook
import kotlin.concurrent.thread

/*
The Metering Service can run a number of different type Meterers which are based on the notary type.
The meterers are called at the configured polling interval to find the unmetered transactions,
create invoices for them, and then send them to the creators of the transaction.
The point of the polling is so that the creation of metering invoices does not hold up
the overall transaction flow. This then gives us the option to send invoices in bulk
and to send them at discreet times if we wish
I'm sure there are plenty of other groovy ways you could do metering, so fill your boots :)
*/

@CordaService
@CordaSerializable
class MeteringService(private val serviceHub: AppServiceHub) : SingletonSerializeAsToken() {

  private val log = loggerFor<MeteringService>()
  private val myIdentity = serviceHub.myInfo.legalIdentities.first()


  @Volatile private var running = true
  private lateinit var meteringThread: Thread

  private val meteringServiceConfig by lazy {
    MeteringServiceConfig.loadConfig()
  }

  private val meterer: AbstractMeterer by lazy {
    val meteringServiceType = meteringServiceConfig.meteringServiceType
    //You can extend this add you add new meterers, or create a factory if you must, and maybe an abstract factory if your performance is being measured on the the lines of code you produce
    when (meteringServiceType) {
      MeteringServiceType.SingleNodeMeterer -> SingleNodeMeterer(serviceHub, meteringServiceConfig)
      else -> {
        throw MeteringServiceException("Invalid Metering Service Type in configuration ${meteringServiceType}")
      }
    }
  }

  init {
    log.info("instantiating Metering Service $myIdentity")

    if (iShouldBeMetering()) {
      log.info("Starting Metering Service $myIdentity")
      if (meteringServiceConfig.meteringNotaryAccountId.isNullOrBlank()) {
        throw MeteringServiceException("Missing account Id in metering service configuration")
      }
      if (meteringServiceConfig.daoPartyName.isNullOrBlank()) {
        throw MeteringServiceException("Missing daoPartyName in metering service configuration")
      }

      log.info("Started Metering Service on $myIdentity")
      addShutdownHook { shutdown() }
      startMetering()
    }
    else{
      log.info("$myIdentity This is not configured to be a metering node - metering service exiting")
    }
  }

  private fun startMetering() {
    val meteringRefreshInterval = meteringServiceConfig.meteringRefreshInterval
    meteringThread = thread(start = true) {
      while (running) {
        log.debug("Metering Service Running $myIdentity")
        meterer.meterTransactions()
        Thread.sleep(meteringRefreshInterval)
      }
    }
  }

  private fun iShouldBeMetering(): Boolean {
    return meteringServiceConfig.meteringPartyName == myIdentity.name.organisation
  }

  fun shutdown() {
    log.info("Metering Service Shutting Down on Node: $myIdentity")
    running = false
    meteringThread.join()
    log.info("Metering Service Finished, having done a great job of metering, have a great day!")
  }
}