/**
 *   Copyright 2018, Cordite Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package io.cordite.commons.utils

object Resources {
  @Throws(IllegalArgumentException::class)
  fun loadResourceAsString(path: String): String {
    val stream = ClassLoader.getSystemClassLoader().getResourceAsStream(path)
        ?: throw IllegalArgumentException("could not locate resource $path")

    return stream.use {
      val scanner = java.util.Scanner(stream).useDelimiter("\\A")
      if (scanner.hasNext()) scanner.next() else ""
    }
  }
}