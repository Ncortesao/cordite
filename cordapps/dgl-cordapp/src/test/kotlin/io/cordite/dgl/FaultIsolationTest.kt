/**
 *   Copyright 2018, Cordite Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package io.cordite.dgl

import io.bluebank.braid.client.BraidClientConfig
import io.bluebank.braid.client.BraidCordaProxyClient
import io.bluebank.braid.core.async.getOrThrow
import io.cordite.dgl.corda.LedgerApi
import io.vertx.core.Vertx
import io.vertx.ext.unit.TestContext
import io.vertx.ext.unit.junit.VertxUnitRunner
import org.junit.After
import org.junit.Ignore
import org.junit.Test
import org.junit.runner.RunWith
import java.net.URI

// TODO: https://gitlab.com/cordite/cordite/issues/194
@Ignore
@RunWith(VertxUnitRunner::class)
class FaultIsolationTest {
  val proxy = BraidCordaProxyClient(
      BraidClientConfig(
          serviceURI = URI("https://localhost:8081/api/ledger/braid"),
          tls = true,
          trustAll = true,
          verifyHost = false), Vertx.vertx())
  val ledger = proxy.bind(LedgerApi::class.java)

  @After
  fun after() {
    proxy.close()
  }

  @Test
  fun step1(context: TestContext) {
    context.async()
    val future = ledger.listTokenTypes()
    val result1 = future.getOrThrow()
    println(result1)
  }

  @Test
  fun step2() {
    val result2 = ledger.wellKnownTagCategories()
    println(result2)
  }

  @Test
  fun step1and2() {
    val future = ledger.listTokenTypes()
    val result1 = future.getOrThrow()
    println(result1)
    val result2 = ledger.wellKnownTagCategories()
    println(result2)
  }
}