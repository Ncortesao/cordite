/**
 *   Copyright 2018, Cordite Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package io.cordite.metering.daostate

import io.cordite.dao.core.BaseModelData
import io.cordite.dao.core.DaoContract
import io.cordite.dao.core.DaoState
import net.corda.core.identity.Party
import net.corda.core.serialization.CordaSerializable

@CordaSerializable
data class MeteringModelData(val meteringTransactionCost: MeteringTransactionCost,
                             val meteringNotaryMembers: Map<String, MeteringNotaryMember>,
                             val meteringFeeAllocation: MeteringFeeAllocation) : BaseModelData() {

  fun cloneWithNewMeteringTransactionCost(newMeteringTransactionCost: MeteringTransactionCost): MeteringModelData {

    if (newMeteringTransactionCost == meteringTransactionCost) {
      throw MeteringModelDataException("you are trying to change the metering transaction cost, but nothing has changed, newMeteringTransactionCost $newMeteringTransactionCost")
    }

    val newMeteringModelData = MeteringModelData(
      meteringTransactionCost = newMeteringTransactionCost,
      meteringNotaryMembers = meteringNotaryMembers,
      meteringFeeAllocation = meteringFeeAllocation)
    return newMeteringModelData
  }

  fun cloneWithNewMeteringFeeAllocation(newMeteringFeeAllocation: MeteringFeeAllocation): MeteringModelData {
    if (newMeteringFeeAllocation == meteringFeeAllocation) {
      throw MeteringModelDataException("you are trying to change the metering fee allocation, but have not changed anything, you new metering fee allocation looks like this $newMeteringFeeAllocation")
    }

    val newMeteringModelData = MeteringModelData(
      meteringTransactionCost = meteringTransactionCost,
      meteringNotaryMembers = meteringNotaryMembers,
      meteringFeeAllocation = newMeteringFeeAllocation)
    return newMeteringModelData
  }

  fun cloneWithUpdatedMeteringNotaryMember(newMeteringNotaryMember: MeteringNotaryMember): MeteringModelData {

    if (meteringNotaryMembers.containsKey(newMeteringNotaryMember.notaryParty.name.toString())) {
      //lets check it's actually been changed
      val existingMeteringNotaryMember = meteringNotaryMembers[newMeteringNotaryMember.notaryParty.name.toString()]

      if (existingMeteringNotaryMember == newMeteringNotaryMember) {
        throw MeteringModelDataException("You are trying to update a metering notary member, but you have not changed any fields, here's what you sent ${newMeteringNotaryMember}")
      }
      val newMeteringNotaryMembers = meteringNotaryMembers.toMutableMap().apply { this.replace(newMeteringNotaryMember.notaryParty.name.toString(), newMeteringNotaryMember) }.toMap()

      val newMeteringModelData = MeteringModelData(
        meteringTransactionCost = meteringTransactionCost,
        meteringNotaryMembers = newMeteringNotaryMembers,
        meteringFeeAllocation = meteringFeeAllocation
      )
      return newMeteringModelData
    }
    throw MeteringModelDataException("you are trying to update a metering notary member that does not exist, you may want to try adding it instead, the member is ${newMeteringNotaryMember.notaryParty.name}")
  }

  fun cloneWithNewMeteringNotaryMember(newMeteringNotaryMember: MeteringNotaryMember): MeteringModelData {
    if (meteringNotaryMembers.containsKey(newMeteringNotaryMember.notaryParty.name.toString())) {
      throw throw MeteringModelDataException("you are trying add a new metering notary member, but they already exist, their name is ${newMeteringNotaryMember.notaryParty.name}")
    }
    val newMeteringNotaryMembers = meteringNotaryMembers + Pair(newMeteringNotaryMember.notaryParty.name.toString(), newMeteringNotaryMember)

    val newMeteringModelData = MeteringModelData(
      meteringTransactionCost = meteringTransactionCost,
      meteringNotaryMembers = newMeteringNotaryMembers,
      meteringFeeAllocation = meteringFeeAllocation
    )
    return newMeteringModelData
  }

  fun cloneWithRemovedMeteringNotaryMember(meteringNotaryParty: Party): MeteringModelData {
    if (!meteringNotaryMembers.containsKey(meteringNotaryParty.name.toString())) {
      throw MeteringModelDataException("you are trying to remove metering notary member ${meteringNotaryParty.nameOrNull()} but it does not exist")
    }
    val newMeteringNotaryMembers = meteringNotaryMembers - meteringNotaryParty.name.toString()

    val newMeteringModelData = MeteringModelData(
      meteringTransactionCost = meteringTransactionCost,
      meteringNotaryMembers = newMeteringNotaryMembers,
      meteringFeeAllocation = meteringFeeAllocation)

    return newMeteringModelData
  }

}


